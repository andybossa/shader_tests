﻿using UnityEngine;
using System.Collections;

public class proximity : MonoBehaviour {
	[SerializeField]
	private float _min, _max;
	[SerializeField]
	private Transform objectTrack;

	private Vector4 cachedColor;

	void Start(){
		cachedColor = renderer.sharedMaterial.GetColor("_MainColor");
	}

	void Update ()
	{
		renderer.sharedMaterial.SetVector( "_PlayerPos" , objectTrack.position );
		renderer.sharedMaterial.SetColor("_MainColor", cachedColor*Random.Range(_min,_max));
	}

}
