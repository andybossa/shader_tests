using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GetPixelsTest : MonoBehaviour {
	[SerializeField]
	private Texture2D _texToSample;
	
	private Color[] _sampledImage;
	private MeshFilter[] _cubes;
	
	// Use this for initialization
	void Start () {
		_sampledImage = _texToSample.GetPixels();
		_cubes = transform.GetComponentsInChildren<MeshFilter>();
		
		foreach(MeshFilter cube in _cubes){
			//print(cube);
		}
		
		print (_sampledImage.Length);
		
		for(int i = 0; i < _sampledImage.Length; i++){
//			_cubes[i].mesh.colors = _sampledImage[i];
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
