Shader "Custom/Lambert Ambient"{
	Properties {
		_Color ("Color", Color) = (1.0,1.0,1.0,1.0)
		_Atten ("Attenuation", Range(0.0,1.0)) = 1.0
	}
	SubShader{
		Pass{
			Tags { "LightMode" = "ForwardBase"} // Special info about each pass, changed to forward rendering
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			//User defined vars
			uniform float4 _Color;
			uniform float _Atten;
			
			//Unity defined vars
			uniform float4 _LightColor0;
			
			//Structs
			struct vertexInput{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};
			
			struct vertexOutput{
				float4 pos : SV_POSITION;
				float4 col : COLOR;
			};
			
			vertexOutput vert(vertexInput v){
				vertexOutput o;
				
				float3 normalDirection = normalize( mul(float4(v.normal,0.0), _World2Object)).xyz;
				float3 lightDirection;
				
				lightDirection = normalize( _WorldSpaceLightPos0.xyz ); // Is direction for directional lighting, is pos for point lights I think
				
				float3 diffuseReflection = _Atten * _LightColor0.xyz * max (0.0, dot(normalDirection, lightDirection));
				float3 lightFinal = diffuseReflection + UNITY_LIGHTMODEL_AMBIENT.xyz; // Take ambient lighting into account.
				// Dot product returns a -1 to 1 float. This is multiplied by the colour to give you the lighting. So if something
				// Is 1, then it's going to be the full intensity of the colour, if it's 0 then it's going to be black.
				// The max() function is to remove any values below zero as these will effect illumination from other lights.
				// Eg. a dot prod of -1 from one light and a 1 from another light will still make the surface black.
				
				
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex); // Still need to send vertex position back to unity
				o.col = float4(lightFinal * _Color.rgb ,1.0); // Defined float4 to add the alpha which isn't being calculated.
				return o;
			}
			
			float4 frag(vertexOutput i) : COLOR
			{
				return i.col;
			}
			ENDCG
		}
	}
}