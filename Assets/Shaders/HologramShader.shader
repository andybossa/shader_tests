﻿Shader "TTL/UI/HologramShader" {
	Properties {
		_Color ("Main Color", Color) = (1,1,1,0.5)
		_Ilace ("Interlace Strength", Range(0,1)) = 1
		_MainTex ("Colour (RGB) Transparency (A)", 2D) = "white" {}
		_IlaceTex ("Interlace Texture", 2D) = "white" {}
		_IlaceScollSpeed ("Interlace Scroll Speed", Range(0,10)) = 2
//		_NoiseTex("Noise Texture", 2D) = "white" {}
		_Offset ("Hologram Offset", Float) = 0.0
		_AlphaClip ("Alpha Clip", Range(0,1)) = 1
	}
	SubShader {	
	Tags { "Queue" = "Transparent" }
	
		Pass { // Main Image
		Blend SrcAlpha OneMinusSrcAlpha
		ZWrite off
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		
		#include "UnityCG.cginc"
		
		sampler2D _MainTex;
		sampler2D _IlaceTex;
		sampler2D _NoiseTex;
		half4 _Color;
		fixed _Ilace;
		fixed _IlaceScollSpeed;
		
		
		struct v2f {
			half4 pos : SV_POSITION;
			half4 color : COLOR;
			half2 uv : TEXCOORD0;
			half2 uv2 : TEXCOORD1;
		};
		
		half4 _MainTex_ST;
		half4 _IlaceTex_ST;
		
		
		v2f vert (appdata_base v){
			v2f o;
			half3 viewDir = normalize(ObjSpaceViewDir(v.vertex));
			half dotProd = 1 - dot(viewDir, v.normal);
			o.color = saturate(dotProd + 0.2);
			o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
			o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
			o.uv2 = TRANSFORM_TEX(v.texcoord, _IlaceTex);		
			return o;
		}
		
		half4 frag (v2f i) : COLOR{
			fixed2 scrolledUV = i.uv2;
			scrolledUV += fixed2(0,_IlaceScollSpeed * _Time);
			half4 texcol = tex2D(_MainTex, i.uv);
			half4 ilace = tex2D(_IlaceTex, scrolledUV);
			half4 noise = tex2D(_NoiseTex, _ScreenParams.xy);
			texcol.a = (texcol.a * _Color.a)-(ilace.a*_Ilace);
			return (texcol * i.color) * noise;
		}
		
		ENDCG
	}
	
	Pass { // Image one
		Blend One One
		ZWrite off
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		
		#include "UnityCG.cginc"
		
		half4 _Color;
		sampler2D _MainTex;
		fixed _Offset;
		fixed _AlphaClip;
		
		struct v2f {
			half4 pos : SV_POSITION;
			half2 uv : TEXCOORD0;
			half4 color : COLOR;
		};
		
		half4 _MainTex_ST;
		
		v2f vert (appdata_base v){
			v2f o;
			half3 viewDir = normalize(ObjSpaceViewDir(v.vertex));
			half dotProd = 1 - dot(viewDir, v.normal);
			o.color = saturate(dotProd + 0.2);
			o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
			o.pos.x += _Offset*0.1f;
			o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
			return o;
		}
		
		half4 frag (v2f i) : COLOR{
			half4 texcol = tex2D(_MainTex, i.uv);
			texcol.rgb *= texcol.a;
			return texcol * i.color * _Color;
		}
		
		ENDCG
	}
	
	Pass { // Image Two
		Blend One One
		ZWrite off
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		
		#include "UnityCG.cginc"
		
		half4 _Color;
		sampler2D _MainTex;
		fixed _Offset;
		fixed _AlphaClip;
		
		struct v2f {
			half4 pos : SV_POSITION;
			half2 uv : TEXCOORD0;
			half4 color : COLOR;
		};
		
		half4 _MainTex_ST;
		
		v2f vert (appdata_base v){
			v2f o;
			half3 viewDir = normalize(ObjSpaceViewDir(v.vertex));
			half dotProd = 1 - dot(viewDir, v.normal);
			o.color = saturate(dotProd + 0.2);
			o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
			o.pos.x -= _Offset*0.1f;
			o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
			return o;
		}
		
		half4 frag (v2f i) : COLOR{
			half4 texcol = tex2D(_MainTex, i.uv);
			texcol.rgb *= texcol.a;
			return texcol * i.color * _Color;
		}
		
		ENDCG
	}
	}
	//FallBack "VertexLit"
}