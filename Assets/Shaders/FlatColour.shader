Shader "Custom/FlatColor" {
	Properties{
		_Color ("Color", Color) = (1.0,1.0,1.0,1.0)
	}
	SubShader {
		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			
			//User degined variables
			uniform float4 _Color;
			
			//Base input structs
			struct VertexInput{
				float4 vertex : POSITION;
			};
			
			struct VertexOutput{
				float4 pos : SV_POSITION;
			};
			
			//Vertex Function
			VertexOutput vert(VertexInput v){
				VertexOutput o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				return o;
			}
			
			//Fragment Function
			float4 frag(VertexOutput i) : COLOR 
			{
				return _Color;
			}
			ENDCG
		}
	}
}