﻿Shader "Custom/Unlit Proximity Alpha Cutoff" {
 
        Properties {
                _MainTexture( "Main Texture (RGBA)" , 2D ) = "white" {}
                _TintColor("Tint Colour", Color) = (0,0,0,0)
                _AlphaClip("Alpha Clip", Float) = 1
                _PlayerPos( "Player position" , Vector ) = (0,0,0,0)
                _EffectScale( "Effect scale" , Float ) = 1
                _Pow("Effect power", Float) = 1
        }
 
        SubShader {
                Pass {
	                Tags { "Queue" = "Overlay" "RenderType"="Overlay"}
	                Blend One One
	                AlphaTest Greater [_AlphaClip]
	                Cull off
	                ZWrite on
	                
                        CGPROGRAM
                        #pragma vertex vert
                        #pragma fragment frag
                        #pragma target 3.0
                        #include "UnityCG.cginc"
 
                        uniform sampler2D _MainTexture;
                        uniform float4 _PlayerPos;
                        uniform fixed _Pow;
                        uniform fixed _EffectScale;
                        uniform half4 _TintColor;
 
                        struct vertexInput {
                                half4 vertex : POSITION;
                                half4 texcoord : TEXCOORD;
                                half4 color : COLOR;
                        };
 
                        struct vertexOutput {
                                half4 position : SV_POSITION;
                                half4 texcoord : TEXCOORD;
                                half4 color : COLOR;
                        };
 
 
                        vertexOutput vert( appdata_base input ) {
                                vertexOutput output;
                                output.texcoord = input.texcoord;
                                output.position = mul( UNITY_MATRIX_MVP , input.vertex );
 
                                half4 vertWorldPos = mul( _Object2World , input.vertex );
                                fixed mask = pow(distance( vertWorldPos, _PlayerPos ),_Pow) * _EffectScale;
                                mask = clamp( mask , 0 , 1 );
 
                                output.color = fixed4( mask , mask, mask , mask );
                               
                                return output;
                        }
 
                        float4 frag( vertexOutput input ) : COLOR {
                        		half4 color = tex2D( _MainTexture, input.texcoord.xy );
                                return ((color * _TintColor) * 1-input.color)*color.a;
                        }
                       
                        ENDCG
                }
        }
        FallBack "Diffuse"
}