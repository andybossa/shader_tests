﻿Shader "Custom/Unlit Proximity Flat Colour Multiply" {
 
        Properties {
                _MainColor("Tint Colour Gradient", 2D) = (0,0,0,0)
                _PlayerPos( "Player position" , Vector ) = (0,0,0,0)
                _EffectScale( "Effect scale" , Float ) = 1
                _Pow("Effect power", Float) = 1
        }
 
        SubShader {
                Pass {
	                Tags { "Queue" = "Overlay" "RenderType"="Overlay"}
	                Blend SrcAlpha OneMinusSrcAlpha
	                Cull off
	                ZWrite off
	                
                        CGPROGRAM
                        #pragma vertex vert
                        #pragma fragment frag
                        #pragma target 3.0
                        #include "UnityCG.cginc"
 
                        uniform half4 _PlayerPos;
                        uniform fixed _EffectScale;
                        uniform fixed _Pow;
                        uniform sampler2D _MainColor;
                        uniform half4 _MainColor_ST;
 
                        struct v2f {
                                half4 position : SV_POSITION;
                                half4 uv : TEXCOORD0;
                                half4 color : COLOR;
                        };
 
 
                        v2f vert( appdata_base v ) {
                                v2f o;
                                
                                o.uv = TRANSFORM_TEX(v.texcoord, _MainColor);
                                o.position = mul( UNITY_MATRIX_MVP , v.vertex );
 
                                half4 vertWorldPos = mul( _Object2World , v.vertex );
                                fixed mask = pow(distance( vertWorldPos,  _PlayerPos ),_Pow) * _EffectScale;
                                mask = clamp( mask , 0 , 1 );
 
                                o.color = fixed4( mask , mask, mask , mask );
                               
                                return o;
                        }
 
                        float4 frag( v2f i ) : COLOR {
                        		half2 uv = (0,i.color.r);
                                return tex2D(_MainColor, i.uv) ;
                        }
                       
                        ENDCG
                }
        }
        FallBack "Diffuse"
}