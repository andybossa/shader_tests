﻿Shader "Custom/BasicDiffuse" {
	Properties {
		_EmissiveColor ( "Emmisive Colour", Color) = (1,1,1,1)
		_AmbientColor ( "Ambient Colour", Color) = (1,1,1,1)
		_MySlider ("My Slider", range(0,10)) = 2
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf BasicDiffuse

		half4 _EmissiveColor;
		half4 _AmbientColor;
		half _MySlider;
		
		inline float4 LightingBasicDiffuse (SurfaceOutput s, fixed3 lightDir, fixed atten){
			float difLight = max(0, dot (s.Normal, lightDir));
			float hLambert = difLight * 0.5 + 0.5; 
			float4 col;
			col.rgb = s.Albedo * _LightColor0.rgb * (hLambert * difLight * atten * 2);
			col.a = s.Alpha;
			return col;
		}
		
		struct Input {
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			half4 c = pow((_EmissiveColor + _AmbientColor), _MySlider);
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
