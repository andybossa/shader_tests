Shader "Custom/Specular" {
	Properties{
		_Color ("Color", Color) = (1.0,1.0,1.0,1.0)
		_SpecColor ("Specular", Color) = (1.0,1.0,1.0,1.0)
		_Shininess ("Shininess", Float) = 10
	}
	SubShader{
		Tags {"Lightmode" = "ForwardBase"}
		Pass {
			CGPROGRAM
			
			#pragma vertex vert
			#pragma fragment frag
			
			//User defined variables
			uniform float4 _Color;
			uniform float4 _SpecColor;
			uniform float _Shininess;
			
			//Unity defined variables
			uniform float4 _lightColor0;
			
			//Structs
			struct vertexInput {
			float4 vertex : POSITION;
			float3 normal : NORMAL;
			};
			
			struct vertexOutput {
				float4 pos : SV_POSITION;
				float4 col : COLOR;
			};
			
			//Vertext Function
			vertexOutput vert(vertexInput v){
				vertexOutput o;
				
				//Vectors
				float3 normalDirection = normalize( mul( float4(v.normal, 0.0), _World2Object ).xyz );
				float3 viewDirection = normalize(float3 (float4 ( _WorldSpaceCameraPos.xyz,1.0) - mul (UNITY_MATRIX_MVP, v.vertex).xyz ) );
				float3 lightDirection;
				float atten = 1.0;
				
				//Lighting
				lightDirection = normalize *(_WorldSpaceLightPos0.xyz);
				
				o.col = float4 (lightDirection ,1.0);
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				return o;
			}
			//Fragment Function
			float4 frag(vertexOutput i) : COLOR
			{
				return i.col;
			}
			
			ENDCG
		}
	}
}